// 1) Опишіть своїми словами як працює метод forEach
// Метод forEach перебирає всі значення масива. Для кожного настпуного елемету масиву метод визиває callback функцію та повертає true чи false. Метод forEact також як і метод filter приймає три значення:
// 1. element - елемент масиву
// 2. index - номер за яким зафіксованний елемент
// 3. array - сам масив який ми перебираємо

// 2) Як очистити масив?\
// Для видалення масиву є два методи:
// 1. Зазначити довжину масиву як 0 (приклад array.length = 0)
// 2. Через метод splice() який приймає два аргументи, перший з якого индексу треба почати видалення, та другой скільки елементів видалити. Для повної очистки масиву треба передати тільки 1 аргумент як 0  (array.splice(0));

// 3) Як можна перевірити, що та чи інша змінна є масивом?
// Для цього ми повинні скористатися методом Array.isArray() та в дужках зазначити змінну. Метод поверне true якщо змінна масив.


// Метод використовуючи forEach

const someArr = [
    1,
    2,
    3,
    "23",
    "asd",
    null,
    true,
    {userName: 20, userAge: 15,},
]

const filterBy = (array, delItems) => {
    let newArray = [];
    let dataType = delItems;
    array.forEach(element => {
        if (dataType !== typeof(element)) {
            newArray.push(element);
        }
    });
    return newArray;
}

console.log("Array withou a string", filterBy(someArr, "string"));
console.log("Array withou an object", filterBy(someArr, "object"));
console.log("Array withou an boolean", filterBy(someArr, "boolean"));
console.log("Array withou an number", filterBy(someArr, "number"));



// Метод використовуючи filter()


const filterBy1 = (array, voidType) => {
    return array.filter(element => typeof(element) !== voidType);
}

console.log("Array without string", filterBy1(someArr, "string"));
console.log("Array without number", filterBy1(someArr, "number"));
console.log("Array without boolean", filterBy1(someArr, "boolean"));
console.log("Array without object", filterBy1(someArr, "object"));
