// Чому для роботи з input не рекомендується використовувати клавіатуру?
// Тому що в input можна вводити дані не тільки за допомогою клавіатури, наприклад люди з інвалідністью які будуть використовувати голосовий ввод при роботі з input.

const btnCollection = document.querySelectorAll(".btn");

const colorKey = (array, DataValue) => {
    array.forEach(element => {
        element.dataset.keycode === DataValue ?  element.classList.toggle("colorPressedKey", true) : element.classList.toggle("colorPressedKey", false);
    });
}

document.body.addEventListener("keydown", (event) => {
    colorKey(btnCollection, event.code);
})

