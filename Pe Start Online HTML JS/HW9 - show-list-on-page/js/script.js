// 1) Опишіть, як можна створити новий HTML тег на сторінці.
// Для цього існує декілька методів:
// 1) Створюємо змінну та пишемо document.createElement("Тут вказуэмо який елемент ми хочемо створити"). Далі, для того шоб додати тег на сторінку використовуємо один з методів:
// 1. append - вставка буде всередену елемента на початок;
// 2. prepand - вставка буде всередину елемента в кінці;
// 3. before - вставка буде до зазначеного елемента;
// 4. after - вставка буде після зазначеного елемента.

// 2) Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.
// Або за допомогою метода insertAdjacentElement("Куди вставляемо", сам елемент). Для цього методу використовуємо:
// 1. beforebegin - вставка буде до зазначеного елемента;
// 2. afterbegin - вставка буде всередену елемента на початок;
// 3. beforeend - вставка буде всередину елемента в кінець;
// 4. afterend - вставка буде після зазначеного елемента.

// 3) Як можна видалити елемент зі сторінки?
// За допомогою метода Node.remove()

const someArr = [
    "some",
    "new",
    "arr",
    "here",
    "hello",
    "world",
]
const showArray = (array, domElement = document.body) => {
    const newUl = document.createElement("ul");
    const newArray = array.map(element => {
        const listItem = `<li>${element}</li>`
        return listItem;
    })
    newUl.innerHTML = newArray.join(" ");
    return domElement.append(newUl);
}
showArray(someArr)