const DarkThemeBtn = document.querySelector(".DarkTheme");
const LightThemeBtn = document.querySelector(".lightTheme");
const themeContainer = document.querySelector(".themes");

themeContainer.addEventListener("click", (e) => {
    if (e.target.classList[0] === "lightTheme") {
        localStorage.removeItem("theme");
        localStorage.setItem("theme", "white");
        return document.querySelector("body").style.backgroundColor = localStorage.getItem("theme");
    }
    localStorage.removeItem("theme");
    localStorage.setItem("theme", "#0c1927");
    document.querySelector("body").style.backgroundColor = localStorage.getItem("theme");
})

document.addEventListener("DOMContentLoaded", () => {
    if (localStorage.getItem("theme"))
        document.querySelector("body").style.backgroundColor = localStorage.getItem("theme");
})