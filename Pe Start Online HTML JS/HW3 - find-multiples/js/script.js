// Теоретичні питання
// Описати своїми словами у кілька рядків, навіщо у програмуванні потрібні цикли.
// Опишіть у яких ситуаціях ви використовували той чи інший цикл в JS.
// Що таке явне та неявне приведення (перетворення) типів даних у JS?

// 1) Для того щоб сробити одну дію декілька фіксовинх раз (приклад з for) або
// поки логічна умова не буде true (приклад while);
// 2) Коли знаю кількість разів яку мені потрібно щось повторити - for. Коли не знаю - while;
// 3) Явне приведення типу це коли ми зменній чи данним задаємо тип (приклад Number(Some value)). Неявне, це коли ми ділимо 10 / "5" (число на строку) та строка приймає тип числа без явного задання типу.

let userNumber = +prompt("Type your number here");

while (!Number.isInteger(userNumber)) {
    userNumber = +prompt('Type integer number');
}

if (userNumber < 5) {
    console.log("Sorry, no numbers");
} else {
    for (let i = 0; i <= userNumber; i++) {
        if(i % 5 === 0) {
            console.log(i);
        }
    }
}


// let n = +prompt("Type your first value (bigger value)");
// let m = +prompt("Type your second value (smaller value)");

// while (n < m) {
//     n = +prompt("Type your first value again (bigger value)");
//     m = +prompt("Type your second value  again(smaller value)");
// }

// NextStep:
// for (let i = m; i <= n; i++) {
//     for (let j = 2; j < i; j ++){
//         if ( i % j === 0) continue NextStep;
//     }
//     console.log(i);
// }