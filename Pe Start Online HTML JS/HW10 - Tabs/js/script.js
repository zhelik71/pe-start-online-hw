const tabMenu = document.querySelector(".tabs");
const tabMenuTitle = Array.from(document.querySelectorAll(".tabs-title"));
const tabContent = Array.from(document.querySelectorAll(".tabs-content > *"));

let currentActiveTab = tabMenuTitle[0];
let currentDisplayedContent = document.querySelector(`.display-none[data-content = ${currentActiveTab.dataset.title}]`);

const changeContent = () => {
    currentDisplayedContent?.classList.add("display-none");
    currentDisplayedContent = document.querySelector(`.display-none[data-content = ${currentActiveTab.dataset.title}]`);
    currentDisplayedContent?.classList.remove("display-none");
}

tabMenu.addEventListener("click", (e) => {
    currentActiveTab.classList.remove("active");
    currentActiveTab = e.target;
    currentActiveTab.classList.add("active");
    changeContent()
})

document.addEventListener("DOMContentLoaded", () => {
    tabMenuTitle[0].classList.add("active");
    currentDisplayedContent?.classList.remove("display-none");
})





// 2nd real
// let currentActiveTab = tabMenuTitle[0];
// let currentActiveContent = tabContent.find(element => element.dataset.content === currentActiveTab.dataset.title);

// const findAndChangeDataContent = () => {
//     currentActiveContent.classList.toggle("display-none", true);
//     currentActiveContent = tabContent.find(element => element.dataset.content === currentActiveTab.dataset.title);
//     currentActiveContent.classList.toggle("display-none", false);
// }

// tabMenu.addEventListener("click", (event) => {
//     currentActiveTab.classList.toggle("active", false);
//     event.target.classList.toggle("active", true);
//     currentActiveTab = event.target;
//     findAndChangeDataContent();
// })

// document.addEventListener("DOMContentLoaded", () => {
//     const firstTab = tabMenuTitle[0];
//     const findFirstTab = tabContent.find(element => element.dataset.content === firstTab.dataset.title)
//     firstTab.classList.add("active");
//     findFirstTab?.classList.remove("display-none");
// })





// 1st realization
// const changeActiveClass = (array, clickedElement) => {
//     array.forEach(element => {
//         if (element === clickedElement) {
//             return element.classList.add("active");
//         }
//         element.classList.toggle("active", false);
//     });
// }

// const changeContent = (dataTitle, array) => {
//     array.forEach(element => {
//         if (element.dataset.content === dataTitle) {
//             return element.classList.remove("display-none")
//         }
//         element.classList.toggle("display-none", true);
//     })
// }

// tabMenu.addEventListener("click", (event) => {
//     console.log(event.target.dataset.title);
//     changeActiveClass(tabMenuTitle, event.target);
//     changeContent(event.target.dataset.title, tabContent);
// })

// document.addEventListener("DOMContentLoaded", () => {
//     const firstTab = tabMenuTitle[0];
//     const findFirstTab = tabContent.find(element => element.dataset.content === firstTab.dataset.title)
//     firstTab.classList.add("active");
//     findFirstTab?.classList.remove("display-none");
// })
