// 1) Опишіть своїми словами, що таке метод об'єкту

// Метод обьекта это функция фнутри объекта которая выполняет какой-то алгоритм действий

// 2) Який тип даних може мати значення властивості об'єкта?

// Абсолютно любоей. Значение объекта принимает все типы данных

// 3) Об'єкт це посилальний тип даних. Що означає це поняття?

// Это означает что объект храниться как ссылка. Его нельзя перезаписать простым способом в другой объект (например: const obj1 = obj2). При таком присвоеннии в obj2 будет записана ссылка на 1 объект и при изменении ключа или значения в объекте 2 оно поменяеться и в исходном.

const askUserData = (message) => {
    let data = prompt(message);
    while (!data) {data = prompt(message)};
    return data;
}

const createNewUser = () => {
    return {
        userName: askUserData("Enter name"),
        userSurname: askUserData("Enter Surname"),
        getLogin () {
            return (this.userName[0] + this.userSurname).toLowerCase();
        },
    }
}


const newUser = createNewUser()
console.log(newUser);
console.log(newUser.getLogin());


