// 1) Опишіть своїми словами різницю між функціями setTimeout() і setInterval()` -
// setTimeout - робить одну дію через якийсь час (якщо не використовувати рекурсію);
// setInterval - повторює одну дію протягом заданого часу;

// 2) Що станеться, якщо в функцію setTimeout() передати нульову затримку? Чи спрацює вона миттєво і чому?
// Не спрацює, нульова затримка, неможливо через те, що setTimeout відправить у стек викликів де він чекатиме своєї черги на виконання;

// 3) Чому важливо не забувати викликати функцію clearInterval(), коли раніше створений цикл запуску вам вже не потрібен?
// Для того, щоб цикл не був вічним і була можливість його зупинити;

const imgCollection = document.querySelectorAll(".image-to-show");
const startBtn = document.querySelector(".startBtn");
const endBtn = document.querySelector(".endBtn")
const control = document.querySelector(".controlInstruments");
let currnetIndex = 0;
let showImages;

const switchImg = (collection) => {
    collection[currnetIndex].classList.toggle("display-none", true);
    currnetIndex++;
    if ((currnetIndex +1 ) > collection.length) {
        currnetIndex = 0;
    }
    collection[currnetIndex].classList.toggle("display-none", false);
}

control.addEventListener("click", (event) => {
    if(event.target.closest(".startBtn")) {
        event.target.toggleAttribute("disabled", true);
        showImages = setTimeout(function startToShow () {
            switchImg(imgCollection);
            showImages = setTimeout(startToShow, 3000);
        }, 3000);
        endBtn.toggleAttribute("disabled", false);
    }
    if(event.target.closest(".endBtn")) {
        clearTimeout(showImages);
        endBtn.toggleAttribute("disabled", true);
        startBtn.innerText = "Resume showing";
        startBtn.toggleAttribute("disabled", false);
    }
})

document.addEventListener("DOMContentLoaded", () => {
    imgCollection[0].classList.remove("display-none");
})


// WORK

// startBtn.addEventListener("click", (event) => {
//     event.target.toggleAttribute("disabled", true);
//     endBtn.classList.toggle("display-none", false);
//     endBtn.toggleAttribute("disabled", false);
//     let startToShow = setTimeout(function next() {
//         switchImg(imgCollection);
//         startToShow = setTimeout(next, 3000);
//     }, 3000);
//     endBtn.addEventListener("click", () => {
//         clearTimeout(startToShow);
//         endBtn.toggleAttribute("disabled", true);
//         startBtn.toggleAttribute("disabled", false)
//     })
// })

// document.addEventListener("DOMContentLoaded", () => {
//     imgCollection[0].classList.remove("display-none");
// })