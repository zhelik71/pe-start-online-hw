// 1) Опишіть своїми словами що таке Document Object Model (DOM).
// DOM це интерфейс (дерево) з влаштованими методами в браузер який дозволяє нам взаємодіяти між JavaScript CSS та HTML.

// 2) Яка різниця між властивостями HTML-елементів innerHTML та innerText?
// InnerHtml - дозволяє нам вставляти елементи тексту разом с HTML розміткою. Наприклад (<h1>HELLO <span>WORLD</span></h1>), в той час як innerText не сприймає HTML теги та виведе їх як текст.

// 3.1) Як можна звернутися до елемента сторінки за допомогою JS?
// Є декілька варіантів:
// 1. В кінці документу в тезі BODY.
// 2. В Head та прописати в ньому defer.
// 3.2) Який спосіб кращий?
// Найкращім є другий варіант. При ньому браузер підгружає наш скрипт асинхронно.

const allPageParagraphs = document.querySelectorAll("p");

allPageParagraphs.forEach(element => {
    element.style.backgroundColor = "#ff0000";
});

const getElementWithId = document.querySelector("#optionsList");

console.log(getElementWithId);
console.log("parrent node of element with ID", getElementWithId.parentElement);
console.log("childNodes of element with ID", getElementWithId.childNodes);

getElementWithId.childNodes.forEach(element => {
    console.log(`it\s ${element.nodeName} with type of ${element}`);
});

const testParagraph = document.querySelector("#testParagraph ");

testParagraph.innerText = "This is a paragraph";

const getChildElementsWithClassMainHeader = document.querySelectorAll(".main-header > *");

getChildElementsWithClassMainHeader.forEach(element => {
    element.classList.add("nav-item");
    console.log(element);
});

console.log(getChildElementsWithClassMainHeader);

const getAllElementsWithClassHeaderTitle = document.querySelectorAll(".header-title");

getAllElementsWithClassHeaderTitle.forEach(element => {
    element.classList.remove("header-title")
})

console.log(getAllElementsWithClassHeaderTitle);