// Поясніть своїми словами, як ви розумієте, що таке деструктуризація і навіщо вона потрібна
// Вона потрібна для мінійікації записів коду а також зручного об'явлення та перейменування всіх змінних в одній строці

// EX1 start

const clients1 = [
    "Гилберт",
    "Сальваторе",
    "Пирс",
    "Соммерс",
    "Форбс",
    "Донован",
    "Беннет",
];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];

const newSet = new Set([...clients1, ...clients2]);
const newArr = [...newSet];

// Ex1 end

// Ex2 start
const characters = [
    {
        name: "Елена",
        lastName: "Гилберт",
        age: 17,
        gender: "woman",
        status: "human",
    },
    {
        name: "Кэролайн",
        lastName: "Форбс",
        age: 17,
        gender: "woman",
        status: "human",
    },
    {
        name: "Аларик",
        lastName: "Зальцман",
        age: 31,
        gender: "man",
        status: "human",
    },
    {
        name: "Дэймон",
        lastName: "Сальваторе",
        age: 156,
        gender: "man",
        status: "vampire",
    },
    {
        name: "Ребекка",
        lastName: "Майклсон",
        age: 1089,
        gender: "woman",
        status: "vempire",
    },
    {
        name: "Клаус",
        lastName: "Майклсон",
        age: 1093,
        gender: "man",
        status: "vampire",
    },
];

const createCharactersShortInfo = (arr) => {
    const newArr = [];
    arr.map((element) => {
        const { name, lastName, age } = element;
        const newObj = {
            name,
            lastName,
            age,
        };
        newArr.push(newObj);
    });
    return newArr;
};

const charactersShortInfo = createCharactersShortInfo(characters);

// Ex2 end

// Ex3 start
const user1 = {
    name: "John",
    years: 30,
};

const { name: имя, years: возраст, isAdmin = false } = user1;
console.log(имя, возраст, isAdmin);
// Ex3 end

// Ex4 start

const satoshi2020 = {
    name: "Nick",
    surname: "Sabo",
    age: 51,
    country: "Japan",
    birth: "1979-08-21",
    location: {
        lat: 38.869422,
        lng: 139.876632,
    },
};

const satoshi2019 = {
    name: "Dorian",
    surname: "Nakamoto",
    age: 44,
    hidden: true,
    country: "USA",
    wallet: "1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa",
    browser: "Chrome",
};

const satoshi2018 = {
    name: "Satoshi",
    surname: "Nakamoto",
    technology: "Bitcoin",
    country: "Japan",
    browser: "Tor",
    birth: "1975-04-05",
};

const fullProfile = { ...satoshi2018, ...satoshi2019, ...satoshi2020 };

console.log(fullProfile);

// Ex4 end

// Ex5 start

const books = [
    {
        name: "Harry Potter",
        author: "J.K. Rowling",
    },
    {
        name: "Lord of the rings",
        author: "J.R.R. Tolkien",
    },
    {
        name: "The witcher",
        author: "Andrzej Sapkowski",
    },
];

const bookToAdd = {
    name: "Game of thrones",
    author: "George R. R. Martin",
};

const addBook = (nativeArr, ...args) => {
    const newArr = [];
    nativeArr.map((el) => {
        const { name, author } = el;
        newArr.push({ name, author });
    });
    const [{ name, author }] = args;
    newArr.push({ name, author });
    return newArr;
};

const newBooksArr = addBook(books, bookToAdd);

// Ex5 end

// Ex6 start

const employee = {
    name: "Vitalii",
    surname: "Klichko",
};

const { name, surname, age, salary } = employee;

const newObj = {
    name,
    surname,
    age,
    salary,
};

console.log(newObj);

// Ex6 end

// Ex7 start

const array = ["value", () => "showValue"];

const [value, showValue] = array;

alert(value); // має бути виведено 'value'
alert(showValue()); // має бути виведено 'showValue'\

// Ex7 end