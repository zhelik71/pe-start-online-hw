class Card {
    constructor(id, userId, title, body, name, userName, email) {
        this.id = id;
        this.userId = userId;
        this.title = title;
        this.body = body;
        this.name = name;
        this.userName = userName;
        this.email = email;
        this.wrapper = document.createElement("div");
        this.delBtn = document.createElement("button");
        this.addPostBtn = document.createElement("button");
    }
    createElement() {
        this.delBtn.classList.add("card__del-btn");
        this.delBtn.innerText = "DELETE";
        this.addPostBtn.innerText = "ADD POST";

        this.delBtn.addEventListener("click", () => {
            fetch(`https://ajax.test-danit.com/api/json/posts/${this.id}`, {
                method: "DELETE",
            })
                .then((res) => {
                    if (res.ok) {
                        this.wrapper.remove();
                    } else {
                        throw new Error();
                    }
                })
                .catch((err) => console.warn(err.message));
        });
        this.wrapper.append(this.delBtn);
        this.wrapper.classList.add("card__wrapper");
        this.wrapper.insertAdjacentHTML(
            "afterbegin",
            `
      <div class="card__wrapper-title">
      <h2>${this.name}</h2>
      <h4>${this.userName}</h4>
      <a>${this.email}</a>
      </div>
      <h3>${this.title}</h3>
      <p>${this.body}</p>
      `
        );
    }
    render(container) {
        this.createElement();
        return container.append(this.wrapper);
    }
}
Promise.all([
    fetch("https://ajax.test-danit.com/api/json/users").then((res) =>
        res.json().then((data) => data)
    ),
    fetch("https://ajax.test-danit.com/api/json/posts").then((res) =>
        res.json().then((data) => data)
    ),
]).then((data) => {
    const [userArr, postsArr] = data;
    let cardArr = document.createElement("div");
    cardArr.classList.add("card");
    postsArr.forEach(({ id, userId, title, body }) => {
        const user = userArr.find((item) => item.id === userId);
        new Card(
            id,
            userId,
            title,
            body,
            user.name,
            user.username,
            user.email
        ).render(cardArr);
    });
    const container = document.querySelector(".container");
    container.classList.remove("loadingMargin");
    document.querySelector(".loader").remove();
    container.append(cardArr);
});