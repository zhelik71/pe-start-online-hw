
const btn = document.querySelector(".find__ip");
const contentContainer = document.querySelector(".content");

const findClientIpAdress = async () => {
    const { ip } = await fetch("https://api.ipify.org/?format=json").then((res) =>
        res.json()
    );
    console.log(ip);
    const data = await fetch(`http://ip-api.com/json/${ip}?fields=1572885`).then(
        (res) => res.json()
    );
    return data;
};
const renderClientsAddress = async (selector) => {
    const { continent, country, region, city, district } =
        await findClientIpAdress();
    selector.insertAdjacentHTML(
        "afterBegin",
        `
  <h2>Continent: ${continent}</h2>
  <h3>Country: ${country}</h3>
  <h4>Region: ${region}</h4>
  <p>City: ${city}</p>
  <p>district:${district}</p>
  `
    );
};
btn.addEventListener("click", (e) => {
    contentContainer.innerHTML = "";
    renderClientsAddress(contentContainer);
});