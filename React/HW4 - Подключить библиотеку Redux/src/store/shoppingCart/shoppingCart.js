import { createSlice } from "@reduxjs/toolkit";

const headerCartReducer = createSlice({
    name: "headerCart",
    initialState: {
        isOpen: false,
    },
    reducers: {
        setIsOpen: (state) => {
            state.isOpen = true;
        },
        setIsClose: (state) => {
            state.isOpen = false;
        },
    },
});

export default headerCartReducer.reducer;

export const { setIsOpen, setIsClose } = headerCartReducer.actions;
