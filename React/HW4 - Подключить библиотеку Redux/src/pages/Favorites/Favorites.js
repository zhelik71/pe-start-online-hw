import { useSelector } from "react-redux";
import Card from "../../Components/Card/Card";
import styles from "./Favorites.module.scss";

const Favorites = (props) => {
    const goods = useSelector((state) => state.goods.goods);
    const goodsAtFavorites = goods.filter((el) => el.inFavorites);
    return (
        <div className={styles.sectionWrapper}>
            <h2>Favorites</h2>
            <div className={styles.goodsWrapper}>
                {goodsAtFavorites.length === 0
                    ? "No items has been added"
                    : goodsAtFavorites.map((el) => (
                        <Card item={el} key={el.EAN} id={el.EAN} />
                    ))}
            </div>
        </div>
    );
};

export default Favorites;
