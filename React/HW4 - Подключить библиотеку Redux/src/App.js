import React, { useEffect } from "react";
import Header from "./Components/Header/Header";
import Modal from "./Components/Modal/Modal.";
import Button from "./Components/Button/Button";
import AppRoutes from "./AppRoutes";
import { useDispatch, useSelector } from "react-redux";
import getGoods from "./store/goods/ASYNCgetGoods/getGoods";
import {
    deleteAllItemsFromCartAC,
    handleAddToCartAC,
} from "./store/goods/goods";
import { setModalClose } from "./store/modalProps/modalProps";
import {
    incrementCounter,
    decrementCounter,
} from "./store/goodsAtCartCounter/goodsAtCartCounter";

const App = () => {
    const dispatch = useDispatch();
    const modalProps = useSelector((state) => state.modalProps.modalProps);
    const isModalOpen = useSelector((state) => state.modalProps.isOpen);

    useEffect(() => {
        dispatch(getGoods());
    }, []);

    const { title, action, direction, header, text } = modalProps;

    return (
        <>
            <Header />
            <AppRoutes />
            {isModalOpen && (
                <Modal
                    header={header}
                    text={
                        "Do you really want to " +
                        action +
                        " " +
                        title +
                        " " +
                        direction +
                        " to cart?"
                    }
                    needCrossBtn={true}
                    actions={
                        <Button
                            text={text}
                            handleClick={
                                action === "add"
                                    ? (e) => {
                                        dispatch(incrementCounter(1));
                                        dispatch(handleAddToCartAC(modalProps.id));
                                        dispatch(setModalClose(false));
                                    }
                                    : (e) => {
                                        dispatch(deleteAllItemsFromCartAC(modalProps.id));
                                        dispatch(decrementCounter(modalProps.qty));
                                        dispatch(setModalClose(false));
                                    }
                            }
                        />
                    }
                />
            )}
        </>
    );
};

export default App;
