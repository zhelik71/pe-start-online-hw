import styles from "./ShoppingCart.module.scss";
import BasketCard from "../../Components/CardInBasket/CardInBasket";
const ShoppingCart = (props) => {
    const {
        itemsInCart,
        deleteFromCartClickHandler,
        goodsAtCart,
        handleAddToCart,
        handleAddToCartModal,
    } = props;
    return (
        <>
            <h2>Cart</h2>
            <div className={styles.goodsWrapper}>
                {itemsInCart ? (
                    itemsInCart.map(
                        (el) =>
                            el.cart === "true" && (
                                <BasketCard
                                    handleAddToCartModal={handleAddToCartModal}
                                    key={el.EAN}
                                    id={el.EAN}
                                    item={el}
                                    qty={el.qtyInOrder}
                                    deleteFromCartClickHandler={deleteFromCartClickHandler}
                                    handleAddToCart={handleAddToCart}
                                />
                            )
                    )
                ) : (
                    <h2>Loading</h2>
                )}
                {goodsAtCart === 0 && <p>No items has been added yet</p>}
            </div>
        </>
    );
};

export default ShoppingCart;

// {goods ? (
//     goods.map((el) => (
//       <Card
//         handleFavoritesClick={handleFavoritesClick}
//         handleAddToCartModal={handleAddToCartModal}
//         ratesClickHandler={ratesClickHandler}
//         item={el}
//         key={el.EAN}
//         id={el.EAN}
//         inFavorites={inFavorites}
//       />
//     ))
//   ) : (
//     <h2>Loading</h2>
//   )}
