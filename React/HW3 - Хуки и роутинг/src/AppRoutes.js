import { Route, Routes } from "react-router-dom";
import Favorites from "./pages/Favorites/Favorites";
import Main from "./Components/Main/Main";
import ShoppingCart from "./pages/ShoppingCart/ShoppingCart";
const AppRoutes = (props) => {
    const {
        isLoaded,
        handleAddToCartModal,
        ratesClickHandler,
        goods,
        openModal,
        handleModalClose,
        handleFavoritesClick,
        inFavorites,
        deleteFromCartClickHandler,
        showHideBasket,
        displayCart,
        goodsAtCart,
        handleAddToCart,
    } = props;
    return (
        <Routes>
            <Route
                path="/"
                element={
                    isLoaded ? (
                        <Main
                            handleAddToCartModal={handleAddToCartModal}
                            ratesClickHandler={ratesClickHandler}
                            goods={goods}
                            openModal={openModal}
                            handleModalClose={handleModalClose}
                            handleFavoritesClick={handleFavoritesClick}
                            inFavorites={inFavorites}
                        />
                    ) : (
                        <h2>Loading</h2>
                    )
                }
            />
            <Route
                path="cart"
                element={
                    <ShoppingCart
                        handleAddToCartModal={handleAddToCartModal}
                        itemsInCart={goods}
                        deleteFromCartClickHandler={deleteFromCartClickHandler}
                        showHideBasket={showHideBasket}
                        displayCart={displayCart}
                        goodsAtCart={goodsAtCart}
                        inFavorites={inFavorites}
                        isLoaded={isLoaded}
                        handleAddToCart={handleAddToCart}
                    />
                }
            />
            <Route
                path="favorites"
                element={
                    <Favorites
                        goods={goods}
                        handleFavoritesClick={handleFavoritesClick}
                        handleAddToCartModal={handleAddToCartModal}
                        ratesClickHandler={ratesClickHandler}
                        inFavorites={inFavorites}
                    />
                }
            />
        </Routes>
    );
};

export default AppRoutes;
