import React from "react";
import styles from "./Favorites.module.scss";
import PropTypes from "prop-types";

const Favorites = (props) => {
    const { inFavorites } = props;
    const { favoritescontainer } = styles;
    return (
        <div className={favoritescontainer}>
            <img
                src="./images/favorites/favorites-checked.png"
                alt="favorite-icon"
            ></img>
            <p>{inFavorites}</p>
        </div>
    );
};

export default Favorites;

Favorites.propTypes = {
    inFavorites: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
};

Favorites.defaultProps = {
    inFavorites: 0,
};
