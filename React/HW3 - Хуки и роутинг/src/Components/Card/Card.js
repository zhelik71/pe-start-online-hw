import React from "react";
import styles from "./Card.module.scss";
import classNames from "classnames";
import PropTypes from "prop-types";

const Card = (props) => {
    const {
        item: { title, image, rate, EAN, color, price, inFavorites },
        ratesClickHandler,
        id,
        handleAddToCartModal,
        handleFavoritesClick,
    } = props;
    const { wrapper, checked, favoritesWrapper } = styles;
    return (
        <div className={wrapper}>
            <div className={favoritesWrapper}>
                <img
                    onClick={(e) => handleFavoritesClick(e, id)}
                    src={
                        inFavorites
                            ? "./images/favorites/favorites-checked.png"
                            : "./images/favorites/favorite-icon.png"
                    }
                    alt="fvorites__picture"
                ></img>
            </div>
            <img src={image} alt="card_goods"></img>
            <h2>{title}</h2>
            <div
                onClick={(e) =>
                    e.currentTarget !== e.target ? ratesClickHandler(e, id, rate) : null
                }
            >
                {
                    <>
                        <span className={classNames("fa fa-star", checked)}></span>
                        <span
                            className={
                                rate >= 1
                                    ? classNames("fa fa-star", checked)
                                    : classNames("fa fa-star")
                            }
                        ></span>
                        <span
                            className={
                                rate >= 2
                                    ? classNames("fa fa-star", checked)
                                    : classNames("fa fa-star")
                            }
                        ></span>
                        <span
                            className={
                                rate >= 3
                                    ? classNames("fa fa-star", checked)
                                    : classNames("fa fa-star")
                            }
                        ></span>
                        <span
                            className={
                                rate >= 4
                                    ? classNames("fa fa-star", checked)
                                    : classNames("fa fa-star")
                            }
                        ></span>
                    </>
                }
            </div>
            <p>{EAN}</p>
            <p>Description</p>
            <p>{color}</p>
            <div>
                <p>{price}</p>
                <button
                    onClick={(e) =>
                        handleAddToCartModal(
                            e,
                            id,
                            title,
                            "add",
                            "to",
                            "Adding item to cart",
                            "add to cart"
                        )
                    }
                >
                    Add to cart
                </button>
            </div>
        </div>
    );
};

export default Card;

Card.propTypes = {
    item: PropTypes.object,
    ratesClickHandler: PropTypes.func,
    id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    handleAddToCartModal: PropTypes.func,
    handleFavoritesClick: PropTypes.func,
};

Card.defaultProps = {
    item: {},
    ratesClickHandler: () => {},
    id: "",
    handleAddToCartModal: () => {},
    handleFavoritesClick: () => {},
};
