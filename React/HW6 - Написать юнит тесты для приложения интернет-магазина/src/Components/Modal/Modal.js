import React from "react";
import styles from "./Modal.module.scss";
import PropTypes from "prop-types";
import { useDispatch } from "react-redux";
import { setModalClose } from "../../store/modalProps/modalProps";

const Modal = (props) => {
    const dispatch = useDispatch();
    const {
        modal__background,
        modal__component,
        modal__close,
        modal__text,
        modal__buttons,
    } = styles;
    const { header, text, needCrossBtn, actions } = props;
    return (
        <div
            onClick={() => dispatch(setModalClose(false))}
            className={modal__background}
            data-testid="modal-root"
        >
            <div className={modal__component} onClick={(e) => e.stopPropagation()}>
                <div>
                    <h2>{header}</h2>
                    {needCrossBtn && (
                        <div
                            className={modal__close}
                            onClick={() => dispatch(setModalClose(false))}
                        ></div>
                    )}
                </div>
                <p className={modal__text}>{text}</p>
                <div className={modal__buttons}>{actions}</div>
            </div>
        </div>
    );
};

export default Modal;

Modal.propTypes = {
    header: PropTypes.string,
    text: PropTypes.string,
    needCrossBtn: PropTypes.bool,
    actions: PropTypes.node,
    handleCloseModal: PropTypes.func,
};

Modal.defaultProps = {
    header: "",
    text: "",
    needCrossBtn: false,
    actions: <></>,
    handleCloseModal: () => {},
};
