import React, { useContext, useEffect, useState } from "react";
import Card from "../Card/Card";
import PropTypes from "prop-types";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { setIsClose } from "../../store/shoppingCart/shoppingCart";
import ThemeContext from "./styles/showCardRotation";

const Main = () => {
    const dispatch = useDispatch();
    const goods = useSelector((state) => state.goods.goods, shallowEqual);
    const [theme, setTheme] = useState("cards");
    const { table, cards } = useContext(ThemeContext);

    useEffect(() => {
        dispatch(setIsClose());
    }, []);
    return (
        <main>
            <h2 style={{ textAlign: "center", margin: "30px" }}> E-Shop</h2>
            <div
                style={{
                    display: "flex",
                    justifyContent: "right",
                    paddingRight: "120px",
                    paddingBottom: "40px",
                }}
            >
                <img
                    style={{ width: "30px" }}
                    src={
                        theme === "cards"
                            ? "./images/cardView/table-line-icon.svg"
                            : "./images/cardView/column-svgrepo-com.svg"
                    }
                    alt="rotation_image"
                    onClick={() =>
                        theme === "cards" ? setTheme("table") : setTheme("cards")
                    }
                />
            </div>

            <div style={theme === "cards" ? cards : table}>
                {goods ? (
                    goods.map((el) => <Card item={el} key={el.EAN} id={el.EAN} />)
                ) : (
                    <h2>Loading</h2>
                )}
            </div>
        </main>
    );
};

export default Main;

Main.propTypes = {
    goods: PropTypes.array,
    ratesClickHandler: PropTypes.func,
    handleAddToCartModal: PropTypes.func,
    handleFavoritesClick: PropTypes.func,
    inFavorites: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.bool,
        PropTypes.number,
    ]),
};

Main.defaultProps = {
    goods: [],
    ratesClickHandler: () => {},
    handleAddToCartModal: () => {},
    handleFavoritesClick: () => {},
    inFavorites: false,
};
