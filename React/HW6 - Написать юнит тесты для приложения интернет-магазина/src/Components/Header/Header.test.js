import { render } from "@testing-library/react";
import { BrowserRouter } from "react-router-dom";
import Header from "./Header";
import { Provider } from "react-redux";
import store from "../../store";
const Component = () => {
    return <Header />;
};

const MockComponent = () => {
    return (
        <Provider store={store}>
            <BrowserRouter>
                <Component />
            </BrowserRouter>
        </Provider>
    );
};

describe("Header renders", () => {
    test("should header renders", () => {
        const { asFragment } = render(<MockComponent />);
        expect(asFragment()).toMatchSnapshot();
    });
});
