import { render, screen, fireEvent } from "@testing-library/react";
import Button from "./Button";

const handleClick = jest.fn();

describe("Render button", () => {
    test("should button render", () => {
        const { asFragment } = render(
            <Button text={"click"} backgroundColor={"black"} />
        );
        expect(asFragment()).toMatchSnapshot();
    });
});

describe("button works", () => {
    test("should button be in the document", () => {
        render(<Button text={"click"} backgroundColor={"black"} />);
        expect(screen.getByText("click")).toBeInTheDocument();
    });
    test("shoudl handleClick works", () => {
        render(
            <Button
                text={"click"}
                backgroundColor={"black"}
                handleClick={handleClick}
            />
        );
        const btn = screen.getByText("click");
        fireEvent.click(btn);

        expect(handleClick).toHaveBeenCalled();
    });
});
