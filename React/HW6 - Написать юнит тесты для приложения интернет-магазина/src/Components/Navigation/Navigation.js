import { NavLink } from "react-router-dom";
import styles from "./Navigation.module.scss";
const Navigation = () => {
    return (
        <nav>
            <ul className={styles.list}>
                <li>
                    <NavLink className={styles.link} to="/">
                        Home
                    </NavLink>
                </li>
                <li>
                    <NavLink className={styles.link} to="/cart">
                        Cart
                    </NavLink>
                </li>
                <li>
                    <NavLink className={styles.link} to="/favorites">
                        Favorites
                    </NavLink>
                </li>
            </ul>
        </nav>
    );
};

export default Navigation;

