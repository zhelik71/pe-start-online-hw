// Jest Snapshot v1, https://goo.gl/fbAQLP

exports[`Card renders should Card render items 1`] = `
<DocumentFragment>
  <div
    class="wrapper"
  >
    <div
      class="favoritesWrapper"
    >
      <img
        alt="fvorites__picture"
        src="./images/favorites/favorite-icon.png"
      />
    </div>
    <img
      alt="card_goods"
    />
    <h2 />
    <div>
      <span
        class="fa fa-star checked"
      />
      <span
        class="fa fa-star"
      />
      <span
        class="fa fa-star"
      />
      <span
        class="fa fa-star"
      />
      <span
        class="fa fa-star"
      />
    </div>
    <p />
    <p>
      Description
    </p>
    <p />
    <div>
      <p />
      <button
        class="addToCartBtn"
      >
        Add to cart
      </button>
    </div>
  </div>
</DocumentFragment>
`;
