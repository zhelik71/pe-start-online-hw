import { combineReducers } from "@reduxjs/toolkit";
import modalPropsReducer from "./modalProps/modalProps";
import goodsReducer from "./goods/goods";
import inFavoritesReducer from "./inFavorites/inFavorites";
import goodsAtCartCounterReducer from "./goodsAtCartCounter/goodsAtCartCounter";
import headerCartReducer from "./shoppingCart/shoppingCart";

const rootReducer = combineReducers({
    modalProps: modalPropsReducer,
    goods: goodsReducer,
    inFavorites: inFavoritesReducer,
    goodsAtCartCounter: goodsAtCartCounterReducer,
    headerCart: headerCartReducer,
});

export default rootReducer;

