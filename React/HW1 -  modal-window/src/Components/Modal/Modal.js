import React from "react";
import styles from "./Modal.module.scss";

class Modal extends React.PureComponent {
    render() {
        const {
            modal__background,
            modal__component,
            modal__close,
            modal__text,
            modal__buttons,
        } = styles;
        const { handleClick, header, text, needCrossBtn, actions } = this.props;
        return (
            <div className={modal__background} onClick={handleClick}>
                <div className={modal__component} onClick={(e) => e.stopPropagation()}>
                    <div>
                        <h2>{header}</h2>
                        {needCrossBtn && (
                            <div className={modal__close} onClick={handleClick}></div>
                        )}
                    </div>
                    <p className={modal__text}>{text}</p>
                    <div className={modal__buttons}>{actions}</div>
                </div>
            </div>
        );
    }
}

export default Modal;
