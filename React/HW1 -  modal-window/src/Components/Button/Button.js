import React from "react";
import styles from "./Button.module.scss";

class Button extends React.PureComponent {
    render() {
        const { text, handleClick, backgroundColor } = this.props;
        const { buttonStyle } = styles;
        return (
            <button
                className={buttonStyle}
                style={{ backgroundColor }}
                onClick={handleClick}
            >
                {text}
            </button>
        );
    }
}

export default Button;
