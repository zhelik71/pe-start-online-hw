import styles from "./App.module.scss";
import React from "react";
import Button from "./Components/Button/Button";
import Modal from "./Components/Modal/Modal.";

const firstModalText =
    "Once you delete this file, it won’t be possible to undo this action. \n Are you sure you want to delete it?";
const secondModalText = "You will be redirected to another page now";

class App extends React.PureComponent {
    state = {
        openModal: true,
        clickedBtn: "",
    };
    handleOpenModalClick = (e) => {
        if (e.target.textContent === "Open first modal") {
            this.setState((prev) => ({
                openModal: !prev.openModal,
                clickedBtn: "1",
            }));
        } else if (e.target.textContent === "Open second modal") {
            this.setState((prev) => ({
                openModal: !prev.openModal,
                clickedBtn: "2",
            }));
        } else {
            this.setState((prev) => ({
                openModal: !prev.openModal,
            }));
        }
    };
    render() {
        const { container } = styles;
        const modalStatus = this.state.openModal;
        const clickedBtn = this.state.clickedBtn;
        return (
            <div className={container}>
                <div>
                    <Button
                        backgroundColor="blue"
                        handleClick={this.handleOpenModalClick}
                        text="Open first modal"
                    />
                    <Button
                        backgroundColor="yellow"
                        handleClick={this.handleOpenModalClick}
                        text="Open second modal"
                    />
                </div>
                {!modalStatus && clickedBtn === "1" && (
                    <Modal
                        handleClick={this.handleOpenModalClick}
                        header="Do you really want to delete this file? "
                        text={firstModalText}
                        needCrossBtn={true}
                        actions={
                            <>
                                <button onClick={this.handleOpenModalClick}>Ok</button>
                                <button onClick={this.handleOpenModalClick}>Cancel</button>
                            </>
                        }
                    />
                )}
                {!modalStatus && clickedBtn === "2" && (
                    <Modal
                        handleClick={this.handleOpenModalClick}
                        header="Really want to open second Modal? "
                        text={secondModalText}
                        needCrossBtn={false}
                        actions={
                            <>
                                <button onClick={this.handleOpenModalClick}>Confirm</button>
                                <button onClick={this.handleOpenModalClick}>Go back</button>
                            </>
                        }
                    />
                )}
            </div>
        );
    }
}

export default App;
