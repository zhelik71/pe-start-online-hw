import React from "react";
import Header from "./Components/Header/Header";
import Main from "./Components/Main/Main";
import Modal from "./Components/Modal/Modal.";
import Button from "./Components/Button/Button";

class App extends React.PureComponent {
    state = {
        isLoaded: false,
        goods: [],
        displayCart: false,
        goodsAtCartCounter: 0,
        openModal: false,
        modalProps: "",
        inFavorites: 0,
    };

    handleCloseModal = (e) => {
        if (
            e.target.closest("div.Modal_modal__background__ySTEE") ||
            e.target.closest("div.Modal_modal__close__1-QlP")
        ) {
            this.setState((prev) => ({
                openModal: !prev.openModal,
            }));
        }
    };

    handleFavoritesClick = (e, id) => {
        const copiedArr = this.state.goods;
        const findElIndex = copiedArr.findIndex(({ EAN }) => EAN === id);
        if (copiedArr[findElIndex].inFavorites) {
            copiedArr[findElIndex].inFavorites = false;
            this.setState((prev) => {
                const nextFavoriteCounter = parseInt(this.state.inFavorites) - 1;
                localStorage.setItem("inFavorites", nextFavoriteCounter);
                return { inFavorites: nextFavoriteCounter };
            });
        } else {
            copiedArr[findElIndex].inFavorites = true;
            this.setState((prev) => {
                const nextFavoriteCounter = parseInt(this.state.inFavorites) + 1;
                localStorage.setItem("inFavorites", nextFavoriteCounter);
                return { inFavorites: nextFavoriteCounter };
            });
        }
        this.setState((prev) => ({
            goods: [...copiedArr],
        }));
        localStorage.setItem("goods", JSON.stringify(this.state.goods));
    };
    showHideBasket = (e) => {
        this.setState((prev) => ({
            displayCart: !prev.displayCart,
        }));
    };
    handleDeleteFromCart = (e, id) => {
        const copiedGoodsArr = this.state.goods.map((el) => {
            if (el.EAN === id && parseFloat(el.qtyInOrder) > 1) {
                el.qtyInOrder = parseFloat(el.qtyInOrder) - 1;
            } else if (el.EAN === id && parseFloat(el.qtyInOrder) === 1) {
                el.qtyInOrder = 0;
                el.cart = "false";
            } else {
                return el;
            }
            return el;
        });
        this.setState((prev) => ({
            goods: [...copiedGoodsArr],
            goodsAtCartCounter: prev.goodsAtCartCounter - 1,
        }));
        localStorage.setItem("goods", JSON.stringify(this.state.goods));
        localStorage.setItem(
            "goodsAtCartCounter",
            JSON.stringify(this.state.goodsAtCartCounter - 1)
        );
    };
    handleModalClose = () => {
        this.setState((prev) => ({
            openModal: !prev.openModal,
        }));
    };
    handleAddToCartModal = (e, id, title) => {
        this.setState((prev) => ({
            openModal: !prev.openModal,
            modalProps: {
                title: title,
                id: id,
            },
        }));
    };
    handleAddToCart = (e, id) => {
        const findAddedElIndex = this.state.goods.map((el) => {
            if (el.EAN === id) {
                switch (el.cart) {
                    case "true":
                        el.qtyInOrder = parseInt(el.qtyInOrder) + 1;
                        this.setState((prev) => {
                            const counter = this.state.goodsAtCartCounter + 1;
                            localStorage.setItem("goodsAtCartCounter", counter);
                            return { goodsAtCartCounter: counter };
                        });
                        break;
                    case "false":
                        el.cart = "true";
                        el.qtyInOrder = "1";
                        this.setState((prev) => {
                            const counter = this.state.goodsAtCartCounter + 1;
                            localStorage.setItem("goodsAtCartCounter", counter);
                            return { goodsAtCartCounter: counter };
                        });
                        break;
                    default:
                        return null;
                }
                return el;
            } else {
                return el;
            }
        });
        this.setState((prev) => ({
            openModal: !prev.openModal,
            goods: [...findAddedElIndex],
        }));

        localStorage.setItem("goods", JSON.stringify(this.state.goods));
    };
    handleRateClick = (e, id) => {
        const clickedEl = e.target.closest("span");
        let counter = 0;
        if (clickedEl.classList.contains("Card_checked__PxZIY")) {
            const checkPrevStars = (prevElementNode) => {
                let prevElement = prevElementNode.nextSibling;
                if (prevElement) {
                    prevElement.classList.remove("Card_checked__PxZIY");
                    checkPrevStars(prevElement);
                    counter -= 1;
                } else {
                    return;
                }
            };
            checkPrevStars(clickedEl);
        } else {
            clickedEl.classList.add("Card_checked__PxZIY");
            const checkPrevStars = (prevElementNode) => {
                let prevElement = prevElementNode.previousSibling;
                if (prevElement) {
                    prevElement.classList.add("Card_checked__PxZIY");
                    checkPrevStars(prevElement);
                    counter += 1;
                } else {
                    return;
                }
            };
            checkPrevStars(clickedEl);
        }
        const findElementIndex = this.state.goods.findIndex(
            ({ EAN }) => EAN === id
        );
        const copiedGoodsArr = this.state.goods;
        copiedGoodsArr[findElementIndex].rate = counter;
        localStorage.setItem("goods", JSON.stringify(copiedGoodsArr));
        this.setState({ goods: [...copiedGoodsArr] });
    };
    async componentDidMount() {
        if (localStorage.getItem("goods")) {
            const items = JSON.parse(localStorage.getItem("goods"));
            const goodsAtCartItems = JSON.parse(
                localStorage.getItem("goodsAtCartCounter")
            );
            const inFavorites = localStorage.getItem("inFavorites");
            this.setState(() => ({
                goods: [...items],
                isLoaded: true,
                displayCart: false,
                goodsAtCartCounter: goodsAtCartItems,
                inFavorites: inFavorites,
            }));
        } else {
            const goodsArray = await fetch("./items.json").then((res) => res.json());
            this.setState({
                goods: [...goodsArray],
                isLoaded: true,
                displayCart: false,
            });
        }
    }
    render() {
        const { title } = this.state.modalProps;
        const displayCart = this.state.displayCart;
        const goodsAtCart = this.state.goodsAtCartCounter;
        const inFavorites = this.state.inFavorites;

        return (
            <>
                <Header
                    showHideBasket={this.showHideBasket}
                    itemsInCart={this.state.goods}
                    deleteFromCartClickHandler={this.handleDeleteFromCart}
                    displayCart={displayCart}
                    goodsAtCart={goodsAtCart}
                    inFavorites={inFavorites}
                />

                {this.state.isLoaded ? (
                    <Main
                        handleAddToCartModal={this.handleAddToCartModal}
                        ratesClickHandler={this.handleRateClick}
                        goods={this.state.goods}
                        openModal={this.state.openModal}
                        handleModalClose={this.handleModalClose}
                        handleFavoritesClick={this.handleFavoritesClick}
                        inFavorites={inFavorites}
                    />
                ) : (
                    <Main />
                )}
                {this.state.openModal && (
                    <Modal
                        header={`Adding item to cart`}
                        text={"Do you really want to add " + title + " to cart?"}
                        needCrossBtn={true}
                        handleCloseModal={this.handleCloseModal}
                        actions={
                            <Button
                                text={"Add to cart"}
                                handleClick={(e) =>
                                    this.handleAddToCart(e, this.state.modalProps.id)
                                }
                            />
                        }
                    />
                )}
            </>
        );
    }
}

export default App;

// const firstModalText =
//   "Once you delete this file, it won’t be possible to undo this action. \n Are you sure you want to delete it?";
// const secondModalText = "You will be redirected to another page now";

// const { container } = styles;
// const modalStatus = this.state.openModal;
// const clickedBtn = this.state.clickedBtn;
// return (
//   <div className={container}>
//     <div>
//       <Button
//         backgroundColor="blue"
//         handleClick={this.handleOpenModalClick}
//         text="Open first modal"
//       />
//       <Button
//         backgroundColor="yellow"
//         handleClick={this.handleOpenModalClick}
//         text="Open second modal"
//       />
//     </div>
//     {!modalStatus && clickedBtn === "1" && (
//       <Modal
//         handleClick={this.handleOpenModalClick}
//         header="Do you really want to delete this file? "
//         text={firstModalText}
//         needCrossBtn={true}
//         actions={
//           <>
//             <button onClick={this.handleOpenModalClick}>Ok</button>
//             <button onClick={this.handleOpenModalClick}>Cancel</button>
//           </>
//         }
//       />
//     )}
//     {!modalStatus && clickedBtn === "2" && (
//       <Modal
//         handleClick={this.handleOpenModalClick}
//         header="Really want to open second Modal? "
//         text={secondModalText}
//         needCrossBtn={false}
//         actions={
//           <>
//             <button onClick={this.handleOpenModalClick}>Confirm</button>
//             <button onClick={this.handleOpenModalClick}>Go back</button>
//           </>
//         }
//       />
//     )}
//   </div>
