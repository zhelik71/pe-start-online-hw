import React from "react";
import styles from "./Button.module.scss";
import PropTypes from "prop-types";

class Button extends React.PureComponent {
    render() {
        const { text, handleClick, backgroundColor } = this.props;
        const { buttonStyle } = styles;
        return (
            <button
                className={buttonStyle}
                style={{ backgroundColor }}
                onClick={handleClick}
            >
                {text}
            </button>
        );
    }
}

Button.propTypes = {
    text: PropTypes.string,
    handleClick: PropTypes.func,
    backgroundColor: PropTypes.string,
};

Button.defaultProps = {
    text: "",
    handleClick: () => {},
    backgroundColor: "",
};

export default Button;
