import React from "react";
import styles from "./CardInBasket.module.scss";
import crossDeleteBtn from "./img/cross-delete.png";
import classNames from "classnames";
import checkedStyle from "../Card/Card.module.scss";
import PropTypes, { number, string } from "prop-types";

class BasketCard extends React.PureComponent {
    render() {
        const { checked } = checkedStyle;
        const { item, qty, deleteFromCartClickHandler } = this.props;
        const {
            shoppingBasketWrapper,
            shoppingBasketTitle,
            crossDelete,
            shoppingBasketPrice,
        } = styles;
        return (
            <div className={shoppingBasketWrapper}>
                <img src={item.image} alt="item_picture"></img>
                <div className={shoppingBasketTitle}>
                    <h2>
                        <span>{qty + "x"}</span> {item.title + " " + item.color}
                    </h2>
                    {
                        <>
                            <span className={classNames("fa fa-star", checked)}></span>
                            <span
                                className={
                                    item.rate >= 1
                                        ? classNames("fa fa-star", checked)
                                        : classNames("fa fa-star")
                                }
                            ></span>
                            <span
                                className={
                                    item.rate >= 2
                                        ? classNames("fa fa-star", checked)
                                        : classNames("fa fa-star")
                                }
                            ></span>
                            <span
                                className={
                                    item.rate >= 3
                                        ? classNames("fa fa-star", checked)
                                        : classNames("fa fa-star")
                                }
                            ></span>
                            <span
                                className={
                                    item.rate >= 4
                                        ? classNames("fa fa-star", checked)
                                        : classNames("fa fa-star")
                                }
                            ></span>
                        </>
                    }
                </div>
                <div>
                    <p className={shoppingBasketPrice}>
                        {parseFloat(item.price) * item.qtyInOrder + "$"}
                    </p>
                    <img
                        onClick={(e) => deleteFromCartClickHandler(e, item.EAN)}
                        className={crossDelete}
                        src={crossDeleteBtn}
                        alt="delete-button"
                    ></img>
                </div>
            </div>
        );
    }
}

export default BasketCard;

BasketCard.propTypes = {
    item: PropTypes.object,
    qty: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    deleteFromCartClickHandler: PropTypes.func,
};

BasketCard.defaultProps = {
    item: {},
    qty: "",
    deleteFromCartClickHandler: () => {},
};
