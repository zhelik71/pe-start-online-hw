import React from "react";
import styles from "./Favorites.module.scss";
import PropTypes, { number, string } from "prop-types";

class Favorites extends React.PureComponent {
    render() {
        const { inFavorites } = this.props;
        const { favoritescontainer } = styles;
        return (
            <div className={favoritescontainer}>
                <img src="./images/favorites/favorites-checked.png"></img>
                <p>{inFavorites}</p>
            </div>
        );
    }
}

export default Favorites;

Favorites.propTypes = {
    inFavorites: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
};

Favorites.defaultProps = {
    inFavorites: 100,
};
