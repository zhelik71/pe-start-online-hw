import React from "react";
import styles from "./Header.module.scss";
import facebook from "./img/facebook.svg";
import gmail from "./img//gmail.svg";
import hangsout from "./img/hangsout.svg";
import twitter from "./img/twitter.svg";
import instagram from "./img/instagram.svg";
import BasketCard from "../CardInBasket/CardInBasket";
import classNames from "classnames";
import Favorites from "../Favorites/Favorites";
import PropTypes, { number, string } from "prop-types";

class Header extends React.PureComponent {
    render() {
        const {
            itemsInCart,
            deleteFromCartClickHandler,
            showHideBasket,
            displayCart,
            goodsAtCart,
            inFavorites,
        } = this.props;
        const {
            body,
            socialIcons,
            container,
            line,
            loginCartWrapper,
            shoppingBasket,
            goodsAtCartCount,
            shoppingBasketWrapper,
        } = styles;
        return (
            <header>
                <div className={line}></div>
                <section className={body}>
                    <div className={container}>
                        <div className={socialIcons}>
                            <a href="#">
                                <img src={facebook} alt="facebook__icon"></img>
                            </a>
                            <a href="#">
                                <img src={gmail} alt="facebook__icon"></img>
                            </a>

                            <a href="#">
                                <img src={hangsout} alt="facebook__icon"></img>
                            </a>

                            <a href="#">
                                <img src={twitter} alt="facebook__icon"></img>
                            </a>

                            <a href="#">
                                <img src={instagram} alt="facebook__icon"></img>
                            </a>
                        </div>
                    </div>
                    <div className={loginCartWrapper}>
                        <Favorites inFavorites={inFavorites} />
                        <button>login / Register</button>
                        <div className={shoppingBasketWrapper}>
                            <img
                                onClick={showHideBasket}
                                src="./images/cart/shopping-cart.png"
                                alt="basket_image"
                            ></img>
                            <p className={goodsAtCartCount}>{goodsAtCart}</p>
                            <div
                                className={classNames(shoppingBasket)}
                                style={{ display: displayCart ? "block" : "none" }}
                            >
                                {itemsInCart.map(
                                    (el) =>
                                        el.cart === "true" && (
                                            <BasketCard
                                                key={el.EAN}
                                                id={el.EAN}
                                                item={el}
                                                qty={el.qtyInOrder}
                                                deleteFromCartClickHandler={deleteFromCartClickHandler}
                                            />
                                        )
                                )}
                            </div>
                        </div>
                    </div>
                </section>
            </header>
        );
    }
}

export default Header;

Header.propTypes = {
    itemsInCart: PropTypes.array,
    deleteFromCartClickHandler: PropTypes.func,
    showHideBasket: PropTypes.func,
    displayCart: PropTypes.bool,
    goodsAtCart: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    inFavorites: PropTypes.oneOfType([number, string]),
};

Header.defaultProps = {
    itemsInCart: [],
    deleteFromCartClickHandler: () => {},
    showHideBasket: () => {},
    displayCart: false,
    goodsAtCart: 0,
    inFavorites: 0,
};
