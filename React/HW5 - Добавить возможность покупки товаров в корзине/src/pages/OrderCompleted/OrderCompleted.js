import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { setIsClose } from "../../store/shoppingCart/shoppingCart";
import styles from "./OrderCompleted.module.scss";

const CompletedOrder = () => {
    const orderData = useSelector((state) => state.goods.orderedGoodsData);
    let finalOrderPrice = 0;
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(setIsClose());
    }, []);
    return (
        <div>
            <h2 className={styles.pageTitle}> ORDER COMPLETED</h2>
            <div>
                <div className={styles.wrapper}>
                    {orderData &&
                        orderData.map((el) => {
                            const price = parseInt(el.price);
                            const qty = el.qty;
                            finalOrderPrice += price * qty;
                            return (
                                <div key={el.EAN} className={styles.cardWrapper}>
                                    <h2 className={styles.cardTitle}>{el.title}</h2>
                                    <img src={el.image} alt={`card_${el.title}`} />
                                    <p className={styles.cardColor}>{el.color}</p>
                                    <p className={styles.cardPrice}>{el.price}</p>
                                    <p className={styles.orderedQty}>Ordered quantity: {qty}</p>
                                </div>
                            );
                        })}
                </div>

                <p
                    className={styles.finalAmount}
                >{`Amount to be paid: ${finalOrderPrice}`}</p>
            </div>
        </div>
    );
};

export default CompletedOrder;
