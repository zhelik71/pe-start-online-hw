import styles from "./ShoppingCart.module.scss";
import BasketCard from "../../Components/CardInBasket/CardInBasket";
import { useDispatch, useSelector } from "react-redux";
import CheckOutForm from "../../Components/Forms/CheckoutForm/CheckoutForm";
import { useEffect } from "react";
import { setIsClose } from "../../store/shoppingCart/shoppingCart";
const ShoppingCart = (props) => {
    const itemsInCart = useSelector((state) => state.goods.goods);
    const goodsAtCart = useSelector((state) => state.goodsAtCartCounter.counter);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(setIsClose());
    }, []);
    return (
        <>
            <h2 style={{ textAlign: "center", margin: "20px" }}>Cart</h2>
            <div className={goodsAtCart !== 0 && styles.goodsWrapper}>
                <div>
                    {goodsAtCart === 0 ? (
                        <p style={{ textAlign: "center" }}>No items has been added yet</p>
                    ) : (
                        <CheckOutForm />
                    )}
                </div>
                <div>
                    {itemsInCart ? (
                        itemsInCart.map(
                            (el) =>
                                el.cart === true && (
                                    <BasketCard
                                        key={el.EAN}
                                        id={el.EAN}
                                        item={el}
                                        qty={el.qtyInOrder}
                                    />
                                )
                        )
                    ) : (
                        <h2>Loading</h2>
                    )}
                </div>
            </div>
        </>
    );
};

export default ShoppingCart;
