import React, { useEffect } from "react";
import Card from "../Card/Card";
import styles from "./Main.module.scss";
import PropTypes from "prop-types";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { setIsClose } from "../../store/shoppingCart/shoppingCart";

const Main = () => {
    const dispatch = useDispatch();
    const goods = useSelector((state) => state.goods.goods, shallowEqual);
    const { goodsWrapper } = styles;

    useEffect(() => {
        dispatch(setIsClose());
    }, []);
    return (
        <main>
            <h2 style={{ textAlign: "center", margin: "30px" }}> E-Shop</h2>
            <div className={goodsWrapper}>
                {goods ? (
                    goods.map((el) => <Card item={el} key={el.EAN} id={el.EAN} />)
                ) : (
                    <h2>Loading</h2>
                )}
            </div>
        </main>
    );
};

export default Main;

Main.propTypes = {
    goods: PropTypes.array,
    ratesClickHandler: PropTypes.func,
    handleAddToCartModal: PropTypes.func,
    handleFavoritesClick: PropTypes.func,
    inFavorites: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.bool,
        PropTypes.number,
    ]),
};

Main.defaultProps = {
    goods: [],
    ratesClickHandler: () => {},
    handleAddToCartModal: () => {},
    handleFavoritesClick: () => {},
    inFavorites: false,
};
