import { createSlice } from "@reduxjs/toolkit";

const goodsAtCartCounterReducer = createSlice({
    name: "goodsAtCartCounter",
    initialState: {
        counter: 0,
    },
    reducers: {
        incrementCounter: (state, action) => {
            state.counter = state.counter + action.payload;
            localStorage.setItem("goodsAtCartCounter", JSON.stringify(state.counter));
        },
        decrementCounter: (state, action) => {
            state.counter = state.counter - action.payload;
            localStorage.setItem("goodsAtCartCounter", JSON.stringify(state.counter));
        },
        setGoodsAtCartCounter: (state, action) => {
            state.counter = action.payload;
        },
        setCheckoutValue: (state, action) => {
            state.counter = action.payload;
            localStorage.setItem("goodsAtCartCounter", 0);
        },
    },
});

export default goodsAtCartCounterReducer.reducer;
export const {
    incrementCounter,
    decrementCounter,
    setGoodsAtCartCounter,
    setCheckoutValue,
} = goodsAtCartCounterReducer.actions;
