const headerBurger = document.querySelector(".header__burger");
const dropMenu = document.querySelector(".header__burger-menu");
let burgerMenuStatus = false;

headerBurger.addEventListener("click", (e) => {
    if (e.target.classList.contains("header__burger-menu-icon-add")) {
        e.target.classList.remove("header__burger-menu-icon-add");
        e.target.classList.add("header__burger-menu-icon-close");
        dropMenu.classList.remove("display-none");
        burgerMenuStatus = true;
    } else if (e.target.closest(".header__burger-menu-icon-close")) {
        headerBurger.classList.remove("header__burger-menu-icon-close");
        headerBurger.classList.add("header__burger-menu-icon-add");
        dropMenu.classList.add("display-none");
        burgerMenuStatus = false;
    }
});

document.addEventListener("click", (e) => {
    if (burgerMenuStatus === true && e.target !== headerBurger) {
        headerBurger.classList.remove("header__burger-menu-icon-close");
        headerBurger.classList.add("header__burger-menu-icon-add");
        dropMenu.classList.add("display-none");
        burgerMenuStatus = false;
    }
});
