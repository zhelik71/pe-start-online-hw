import gulp from "gulp";
import clean from "gulp-clean";
import dartSass from "sass";
import gulpSass from "gulp-sass";
import autoprefixer from "gulp-autoprefixer";
import purgecss from "gulp-purgecss";
import cleanCSS from "gulp-clean-css";
import concat from "gulp-concat";
import htmlmin from "gulp-htmlmin";
import browserSync from "browser-sync";
import imagemin from "gulp-imagemin";
import terser from "gulp-terser";
const BS = browserSync.create();
const sass = gulpSass(dartSass);

export const minifyImg = () => {
    gulp.src("./src/img/**/*").pipe(imagemin()).pipe(gulp.dest("dist/img"));
};

export const minifyHtml = () => {
    return gulp
        .src("src/*.html")
        .pipe(htmlmin({ collapseWhitespace: true }))
        .pipe(gulp.dest("./dist"));
};

export const concatJs = () => {
    return gulp
        .src("./src/js/**/*.js")
        .pipe(concat("all.js"))
        .pipe(terser())
        .pipe(gulp.dest("./dist/JS"));
};

export const minifyCss = () => {
    return gulp
        .src("./src/styles/**/*.css")
        .pipe(cleanCSS({ compatibility: "ie8" }))
        .pipe(gulp.dest("./dist/styles/CSS"));
};

// export const filterCss = () => {
//   return gulp
//     .src("./src/styles**/*.css")
//     .pipe(
//       purgecss({
//         content: ["./src/**/*.html"],
//       })
//     )
//     .pipe(gulp.dest("./src/"));
// };
export const prefixCss = () => {
    return gulp
        .src("./src/styles/main.css")
        .pipe(
            autoprefixer({
                cascade: false,
            })
        )
        .pipe(gulp.dest("./src/styles"));
};

export const cleanFolder = () => {
    return gulp
        .src(
            [
                "./dist/styles/**/*",
                "./dist/JS/**/*",
                "./dist/*.html",
                "./src/js/*.js",
                "./src/styles/*.css",
            ],
            { read: false }
        )
        .pipe(clean());
};

export const convertToCss = () => {
    return gulp
        .src("./src/styles/scss/**/*.scss")
        .pipe(sass().on("error", sass.logError))
        .pipe(gulp.dest("./src/styles"));
};

export const build = gulp.series(
    cleanFolder,
    convertToCss,
    // filterCss,
    prefixCss,
    concatJs,
    minifyCss,
    minifyHtml
);

export const dev = () => {
    minifyImg();
    BS.init({
        server: {
            baseDir: "./dist",
        },
    });
    gulp.watch(
        ["./src/styles/scss/**/*", "./src/js/main/**/*", "./src/**/*.html"],
        gulp.series(build, (done) => {
            BS.reload();
            done();
        })
    );
};
